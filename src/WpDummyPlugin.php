<?php
declare(strict_types = 1);

namespace ErikPohler\WpDummyPlugin;

/**
 * 
 * WpDummyPlugin class
 * @category class
 * @author erikpoehler
 * @package WpDummyPlugin
 */
final class WpDummyPlugin {
    public function __construct() {
        
    }
    
    public function run() {
        
    }
}