<?php
declare(strict_types = 1);
/*
 Plugin Name: WP Dummy Plugin
 Plugin URI: https://teuton.mx/dummy-plugin
 Description: Really just a dummy plugin, some sort of a placeholder
 Version: 0.0.1
 Author: Teuton Digital
 Author URI: https://teuton.mx/
 License: BSD-3-Clause
 Text Domain: dummy
 */
require_once 'vendor/autoload.php';

$wpDummyPlugin = new ErikPohler\WpDummyPlugin\WpDummyPlugin();
$wpDummyPlugin->run();